﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Fragmen.Startup))]
namespace Fragmen
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
