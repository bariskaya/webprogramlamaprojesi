﻿ 
$('.owl-item1').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ['<i class="glyphicon glyphicon-chevron-left"></i>', '<i class="glyphicon glyphicon-chevron-right"></i>'],
    dots: false, 
    responsive: {
        0: {
            items: 2
        },
        320: {
            items: 3
        },
        600: {
            items: 4
        },
        767: {
            items: 1
        } 
    }
}) 
